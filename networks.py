'''
Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography
written by Marcel Schloz (schlozma@hu-berlin.de)
all rights reserved
'''

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal


class autoencoder(nn.Module):
    """
    A Convolutional Autoencoder Model for Content Compression.

    The Autoencoder model is composed of an encoder and a decoder.
    Convolutional layers are used in the encoder part to compress 
    the intermediate reconstruction V_t(r) and transposed convolutional
    layers are used in the decoder part to decompress the compressed 
    representation z_t. Once trained successfully, only the encoder 
    part is used in the RNN model.

    References
    ----------
    - Schloz et. al., https://arxiv.org/abs/2203.15413
    """
    def __init__(self, Params):
        """
        Initialize the autoencoder model and its
        encoder and decoder component.

        Args
        ----
        - Params: various parameters that determine the model architecture, 
          such as kernel size, # of channels, etc.. More information on the 
          the parameter specifications is given in initialization.py.
        """
        super(autoencoder, self).__init__()

        self.Params = Params
        self.encoder = encoder_network(Params)
        self.decoder = decoder_network(Params)

    def save(self, it):
        """
        Save the model weights of the encoder and decoder component 
        at every it'th iteration.

        Args
        ----
        - it: current iteration of the model training.
        """
        if it % self.Params.save_interval == 0:
            torch.save(self.encoder.cpu().state_dict(), self.Params.save_path + "/network_weights/encoder_it" + str(it) + ".pt")
            self.encoder.cuda()
            torch.save(self.decoder.cpu().state_dict(), self.Params.save_path + "/network_weights/decoder_it" + str(it) + ".pt")
            self.decoder.cuda()

    def forward(self, X):
        """
        Run the autoencoder model on the minibatch of images `X`, 
        i.e. the intermediate reconstructions V_t(r).

        Args
        ----
        - X: a 4D Tensor of shape (B, H, W, C). The minibatch
          of images.

        Returns
        -------
        - X_pred: a 4D Tensor of shape (B, H, W, C). The minibatch
          of predicted images.
        """
        X_comp = self.encoder(X)
        X_pred = self.decoder(X_comp)

        return X_pred


class encoder_network(nn.Module):
    """
    The Encoder part of the Autoencoder Model.

    Convolutional layers followed by leaky Relu nonlinear functions
    are used in the encoder part to compress the intermediate 
    reconstruction V_t(r). The network architecture is determined
    by the input parameters. Once trained successfully, only 
    this encoder part is used in the RNN model.

    References
    ----------
    - Schloz et. al., https://arxiv.org/abs/2203.15413
    """
    def __init__(self, Params):
        """
        Initialize the encoder network.

        Args
        ----
        - Params: various parameters that determine the network architecture, 
          such as kernel size, # of channels, etc.. More information on the 
          the parameter specifications is given in initialization.py.
        """
        super(encoder_network, self).__init__()

        self.Params = Params
        self.encoder = nn.ModuleList([])
        self.createEncoder()

    def createEncoder(self):
        """
        Create each convolutional layer in the encoder network. 
        """
        in_channel = 2
        for depth_layer, k_size in enumerate(self.Params.kernel_sizes):
            pad = int(k_size/2)
            out_channel = self.Params.channels[depth_layer]
            self.encoder.append(nn.Conv2d(in_channels = in_channel, out_channels = out_channel, kernel_size = k_size, stride = 2, padding = pad))
            in_channel = out_channel

    def forward(self, X):
        """
        Run the encoder network on the minibatch of images `X`, 
        i.e. the intermediate reconstructions V_t(r).

        Args
        ----
        - X: a 4D Tensor of shape (B, H, W, C). The minibatch
          of images.

        Returns
        -------
        - X: a 4D Tensor of shape (B, H, W, C). The minibatch
          of compressed images.
        """
        for depth_layer, channel in enumerate(self.Params.channels):
            X = F.leaky_relu(self.encoder[depth_layer](X))

        return X


class decoder_network(nn.Module):
    """
    The Decoder part of the Autoencoder Model.

    Transposed convolutional layers followed by leaky Relu 
    nonlinear functions are used in the decoder part to uncompress 
    the compressed representation z_t. The network architecture 
    is determined by the input parameters. This decoder part is 
    only used in the training of the autoencoder.

    References
    ----------
    - Schloz et. al., https://arxiv.org/abs/2203.15413
    """
    def __init__(self, Params):
        """
        Initialize the decoder network.

        Args
        ----
        - Params: various parameters that determine the network architecture, 
          such as kernel size, # of channels, etc.. More information on the 
          the parameter specifications is given in initialization.py.
        """
        super(decoder_network, self).__init__()

        self.Params = Params
        self.decoder = nn.ModuleList([])
        self.createDecoder()

    def createDecoder(self):
        """
        Create each transposed convolutional layer in the decoder network. 
        """
        out_channels = self.Params.channels[:-1]
        out_channels.insert(0,2)
        for depth_layer, k_size in reversed(list(enumerate(self.Params.kernel_sizes))):
            pad = int(k_size/2)
            in_channel = self.Params.channels[depth_layer]
            out_channel = out_channels[depth_layer]
            self.decoder.append(nn.ConvTranspose2d(in_channels = in_channel, out_channels = out_channel, kernel_size = k_size, stride = 2, padding = pad))

    def forward(self, X):
        """
        Run the decoder network on the minibatch of images `X`, 
        i.e. the compressed images z_t.

        Args
        ----
        - X: a 4D Tensor of shape (B, H, W, C). The minibatch
          of compressed images.

        Returns
        -------
        - X: a 4D Tensor of shape (B, H, W, C). The minibatch
          of decompressed images.
        """
        out_channels = self.Params.channels[:-1]
        out_channels.insert(0,2)
        for depth_layer, channel in reversed(list(enumerate(out_channels))):
            o_size = int(self.Params.input_dimension/(2**depth_layer))
            if(depth_layer != 0):
                X = F.leaky_relu(self.decoder[((len(out_channels)-1) - depth_layer)](X, output_size = [1, channel, o_size, o_size]))
            else:
                X = self.decoder[((len(out_channels)-1) - depth_layer)](X, output_size = [1, channel, o_size, o_size])

        return X


class rnn(nn.Module):
    """
    A Recurrent Neural Network (RNN) for the Generation of Scan Sequences.

    The RNN is composed of an encoder network for the image input, 
    a combiner network for combining image and location input, 
    GRU cells for the integration of previous memory and a locator network 
    for the generation of scan positions.

    References
    ----------
    - Schloz et. al., https://arxiv.org/abs/2203.15413
    """
    def __init__(self, Params):
        """
        Initialize the RNN.

        Args
        ----
        - Params: various parameters that determine the network architecture, 
          such as hidden layer size, # of stacked layers, etc.. More information 
          on the the parameter specifications is given in initialization.py.
        """
        super(rnn, self).__init__()

        self.Params = Params
        self.encoder = encoder_network(self.Params)
        self.combiner = combining_network(self.Params)
        self.gru = nn.GRU(self.Params.hidden_size,self.Params.hidden_size, num_layers = self.Params.stacked_layers)
        self.locator = location_network(self.Params)

    def load(self):
        """
        Load the model weights of the encoder network.
        """
        self.encoder.load_state_dict(torch.load(self.Params.data_path + "/network_weights/encoder_input.pt"))

    def save(self, it):
        """
        Save the model weights of the individual components 
        at every it'th iteration.

        Args
        ----
        - it: current iteration of the model training.
        """
        if it % self.Params.save_interval == 0:
            torch.save(self.combiner.cpu().state_dict(),self.Params.save_path + "/network_weights/combiner_it" + str(it) + ".pt")
            self.combiner.cuda()
            torch.save(self.gru.cpu().state_dict(),self.Params.save_path + "/network_weights/gru_it" + str(it) + ".pt")
            self.gru.cuda()
            torch.save(self.locator.cpu().state_dict(),self.Params.save_path + "/network_weights/locator_it" + str(it) + ".pt")
            self.locator.cuda()

    def forward(self, X, locs, hidden):
        """
        Run the RNN for one timestep t using the input information and hidden state.

        Args
        ----
        - X: a 4D Tensor of shape (B, H, W, C). The minibatch
          of images.
        - locs: a 2D tensor of shape (B, 2). The sub-sequence of 
          scan positions with the coordinates [[x_0, y_0],...,[x_M, y_M]] 
          for the previous timestep `t-1`.
        - hidden: a 2D tensor of shape (B, hidden_size). The hidden
          state vector for the previous timestep `t-1`.

        Returns
        -------
        - hidden_new: a 2D tensor of shape (B, hidden_size). The hidden
          state vector for the current timestep `t`.
        - mus_new: a 2D tensor of shape (B, 2). The mean that parametrizes
          the Gaussian policy.
        """
        X_comp = self.encoder(X)
        X_comp.detach()
        comb = self.combiner(locs, X_comp)
        output_new, hidden_new = self.gru(comb, hidden)
        mus_new = self.locator(output_new[-1])

        return hidden_new, mus_new


class imitator(rnn):
    def __init__(self, Params):
        super(imitator, self).__init__(Params)
        self.Params = Params

    def forward(self, X, locs, hidden):

        return super(imitator, self).forward(X, locs, hidden)


class tuner(rnn):
    def __init__(self, Params):
        super(tuner, self).__init__(Params)

        self.Params = Params

    def load(self):
        super(tuner, self).load()
        self.combiner.load_state_dict(torch.load(self.Params.data_path + "/network_weights/combiner_input.pt"))
        self.gru.load_state_dict(torch.load(self.Params.data_path + "/network_weights/gru_input.pt"))
        self.locator.load_state_dict(torch.load(self.Params.data_path + "/network_weights/locator_input.pt"))

    def forward(self, X, locs, hidden):
        hidden_new, mus_new = super(tuner, self).forward(X, locs, hidden)
        locs_new = Normal(mus_new, self.Params.policy_sigma).sample()
        log_pi = torch.sum(Normal(mus_new, self.Params.policy_sigma).log_prob(locs_new), dim = 2)

        return hidden_new, locs_new, log_pi


class tester(rnn):
    def __init__(self, Params):
        super(tester, self).__init__(Params)
        self.Params = Params

    def load(self):
        super(tester, self).load()
        self.combiner.load_state_dict(torch.load(self.Params.data_path + "/network_weights/combiner_input.pt"))
        self.gru.load_state_dict(torch.load(self.Params.data_path + "/network_weights/gru_input.pt"))
        self.locator.load_state_dict(torch.load(self.Params.data_path + "/network_weights/locator_input.pt"))

    def forward(self, X, locs, hidden):

        return super(tester, self).forward(X, locs, hidden)


class combining_network(nn.Module):
    def __init__(self, Params):
        super(combining_network, self).__init__()

        self.Params = Params
        self.fc_l = nn.Linear(int(2 * Params.subsequence_length), int(Params.hidden_size / 2))
        self.fc_X = nn.Linear(int(8 * 8 * Params.input_dimension), int(Params.hidden_size / 2))
        self.fc_combi = nn.Linear(Params.hidden_size, Params.hidden_size)


    def forward(self, locations, X):

        L_inp = locations.view(-1, 2 * self.Params.subsequence_length)
        L_inp = F.leaky_relu(self.fc_l(L_inp))
        X_inp = X.view(-1, 8 * 8 * self.Params.input_dimension)
        X_inp = F.leaky_relu(self.fc_X(X_inp))

        inp = torch.cat((L_inp, X_inp), 1)
        inp = F.leaky_relu(self.fc_combi(inp))
        inp = inp.view(1, self.Params.batch_size, self.Params.hidden_size)

        return inp


class location_network(nn.Module):
    """
    The locator network of the RNN.

    Transposed convolutional layers followed by leaky Relu 
    nonlinear functions are used in the decoder part to uncompress 
    the compressed representation z_t. The network architecture 
    is determined by the input parameters. This decoder part is 
    only used in the training of the autoencoder.

    References
    ----------
    - Schloz et. al., https://arxiv.org/abs/2203.15413
    """
    def __init__(self, Params):
        super(location_network, self).__init__()

        self.Params = Params
        self.fc = nn.Linear(Params.hidden_size, int(2 * Params.subsequence_length))

    def forward(self, hidden):

        mus = F.tanh(self.fc(hidden))
        mus = mus.view(self.Params.batch_size, self.Params.subsequence_length, 2)

        return mus



