'''
Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography
written by Marcel Schloz (schlozma@hu-berlin.de)
all rights reserved
'''

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader, Sampler, SubsetRandomSampler

import numpy as np
import pickle

from networks import autoencoder, imitator, tuner
from initialization import autoencoder_data_loading, rnn_data_loading
from reconstruction import partial_reconstruction
from auxiliary import initialize, save_results
from reward import generate_reward


def run_training(Params):
    if Params.execution_model == "autoencoder":
        run_autoencoding(Params)
    if Params.execution_model == "imitator":
        run_imitating(Params)
    if Params.execution_model == "tuner":
        run_tuning(Params)


def run_testing(Params):
    if Params.execution_model == "autoencoder":
        run_test_autoencoding(Params)
    if Params.execution_model == "imitator" or "tuner":
        run_test_rnn(Params)


def run_autoencoding(Params):
    """
    A Convolutional Autoencoder Model for Content Compression.

    The Autoencoder model is composed of an encoder and a decoder.
    Convolutional layers are used in the encoder part to compress 
    the intermediate reconstruction V_t(r) and transposed convolutional
    layers are used in the decoder part to decompress the compressed 
    representation z_t. Once trained successfully, only the encoder 
    part is used in the RNN model.

    References
    ----------
    - Schloz et. al., https://arxiv.org/abs/2203.15413
    """
    """
    Initialize the training of the autoencoder model.

    Args
    ----
    - Params: various parameters that determine the training mechanism, 
      such as learning rate, # of iterations, etc.. More information on the 
      the parameter specifications is given in initialization.py.
    """
    #Generate the autoencoder model
    model = autoencoder(Params)
    #Transfer the model from host (CPU) to device (NVIDIA GPU)
    model.cuda()
    #Retrieve the weights of the autoencoder model
    model_parameters = list(model.parameters()) 
    #Specify the optimization algorithm
    optimizer = optim.Adam(model_parameters, lr = Params.learning_rate, amsgrad = True)
    #Specify the loss function
    mse_loss = nn.MSELoss()
    #Retrieve the training data
    data = autoencoder_data_loading(Params)

    """
    Train the autoencoder model on the data.
    """
    for it in range(Params.iterations):
        #Load some data with random sampling
        indices = (np.random.rand(Params.batch_size) * len(data)).astype(int).tolist()
        data_sampler = SubsetRandomSampler(indices)
        dataloader = DataLoader(data, batch_size = Params.batch_size, sampler = data_sampler, num_workers = Params.batch_size)

        save_loss = 0.0

        for epoch, data_batch in enumerate(dataloader):
            #Retrieve ground truth reconstructions
            groundTruth = data_batch[0]
            groundTruth_d = groundTruth.cuda()
            #Retrieve test reconstructions
            X = data_batch[1]
            X_d = X.cuda()
            #Apply the model to the test reconstructions
            X_d = model(X_d)
            #Form the loss function
            loss = mse_loss(X_d, groundTruth_d)
            save_loss += loss.cpu().data.numpy()
            #Generate gradients and update model weights
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        save_results(save_loss, model, Params, it)          


def run_imitating(Params):
        #Generate the RNN model
        model = imitator(Params)
        #Transfer the model from host (CPU) to device (NVIDIA GPU)
        model.cuda()
        #Retrieve the weights of the RNN model
        model_parameters = list(model.parameters())
        #Specify the optimization algorithm
        optimizer = optim.Adam(model_parameters, lr=Params.learning_rate, amsgrad = True)
        #Specify the loss function
        mse_loss = nn.MSELoss()
        #Retrieve the training data
        data = rnn_data_loading(Params)

        """
        Train the RNN model on the data.
        """
        #Load the weights of the encoder network.
        model.load()

        for it in range(Params.iterations):
            scan_sequences = []
            save_loss = 0.0
            stacked_locs = []
            stacked_locs_ref = []
            #Load some data with random sampling
            indices = (np.random.rand(Params.batch_size) * len(data)).astype(int).tolist()
            data_sampler = SubsetRandomSampler(indices)
            dataloader = DataLoader(data, batch_size = Params.batch_size, sampler = data_sampler, num_workers = Params.batch_size)

            for epoch, data_batch in enumerate(dataloader):

                #Retrieve scan positions (normalized [x,y]-coordinates)
                locs_batch = data_batch[0]
                #Retrieve folders with the training data
                dps_batch = data_batch[1]
                psis_re_batch = data_batch[2]
                psis_im_batch = data_batch[3]
                #Creates initial hidden state and converts first scan positions into tensor
                locs, hidden = initialize(locs_batch, Params)
                #Performs a reconstruction with a sub-set of diffraction patterns using ROP
                X, scan_sequences = partial_reconstruction(locs, scan_sequences, dps_batch, psis_re_batch, psis_im_batch, Params, 0)
                #Transfer some tensors from host (CPU) to device (NVIDIA GPU)
                locs_d = locs.cuda()
                X_d = X.cuda()
                hidden_d = hidden.cuda()

                for subseq in range(Params.subsequences - 1):
                    #Apply the model to the input data and hidden state
                    hidden_d, locs_d = model(X_d, locs_d, hidden_d)
                    locs = locs_d.cpu()
                    stacked_locs.append(locs)
                    #Retrieve the ground truth batch of scan positions (normalized [x,y]-coordinates)
                    stacked_locs_ref.append(locs_batch[:,(Params.subsequence_length * (subseq + 1)):(Params.subsequence_length * (subseq + 2)),:].float())

                    if(subseq < (Params.subsequences - 2) ):
                        X, scan_sequences = partial_reconstruction(locs, scan_sequences, dps_batch, psis_re_batch, psis_im_batch, Params, (subseq + 1))
                        X_d = X.cuda()

                #Form the loss function
                loss = mse_loss(torch.stack(stacked_locs), torch.stack(stacked_locs_ref))
                save_loss += loss.data.numpy()
                #Generate gradients and update model weights
                optimizer.zero_grad()
                loss.backward(retain_graph = True)
                optimizer.step()

            save_results(save_loss, model, Params, it, scan_sequences)


def run_tuning(Params):

    #Generate the RNN model
    model = tuner(Params)
    #Transfer the model from host (CPU) to device (NVIDIA GPU)
    model.cuda()
    #Retrieve the weights of the RNN model
    model_parameters = list(model.parameters())
    #Specify the optimization algorithm
    optimizer = optim.Adam(model_parameters, lr=Params.learning_rate, amsgrad = True)
    #Specify the loss function
    mse_loss = nn.MSELoss()
    #Retrieve the training data
    data = rnn_data_loading(Params)

    """
    Train the RNN model on the data.
    """
    #Load the weights of the encoder network.
    model.load()

    for it in range(Params.iterations):

        scan_sequences = []
        log_pis = []
        #Load some data with random sampling
        indices = (np.random.rand(Params.batch_size) * len(data)).astype(int).tolist()
        data_sampler = SubsetRandomSampler(indices)
        dataloader = DataLoader(data, batch_size = Params.batch_size, sampler = data_sampler, num_workers = Params.batch_size)

        for epoch, data_batch in enumerate(dataloader):
            #Retrieve scan positions (normalized [x,y]-coordinates)
            locs_batch = data_batch[0]
            #Retrieve folders with the training data
            dps_batch = data_batch[1]
            psis_re_batch = data_batch[2]
            psis_im_batch = data_batch[3]
            #Creates initial hidden state and converts first scan positions into tensor
            locs, hidden = initialize(locs_batch, Params)
            #Performs a reconstruction with a sub-set of diffraction patterns using ROP
            X, scan_sequences = partial_reconstruction(locs, scan_sequences, dps_batch, psis_re_batch, psis_im_batch, Params, 0)
            #Transfer some tensors from host (CPU) to device (NVIDIA GPU)
            locs_d = locs.cuda()
            X_d = X.cuda()
            hidden_d = hidden.cuda()

            for subseq in range(Params.subsequences - 1):

                hidden_d, locs_d, log_pi_d = model(X_d, locs_d, hidden_d)
                locs = locs_d.cpu()
                #Performs a reconstruction with a sub-set of diffraction patterns
                X, scan_sequences = partial_reconstruction(locs , scan_sequences, dps_batch, psis_re_batch, psis_im_batch, Params, (subseq + 1))
                X_d = X.cuda()
                log_pi = log_pi_d.cpu()
                log_pis.append(log_pi)

            log_pis =  torch.stack(log_pis).transpose(1,0)
            reward = generate_reward(scan_sequences, Params)
            #Form the loss function
            loss = torch.mean(torch.sum( torch.sum( -log_pis * reward, dim = 2), dim = 1), dim = 0)
            #Generate gradients and update model weights
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            save_results(reward, model, Params, it, scan_sequences)

