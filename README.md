# Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography

This is an implementation of [Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography](https://www.nature.com/articles/s41598-023-35740-1) by *Marcel Schloz*.

<p align="center">
 <img src="./misc/Experiment.png" alt="Drawing", width=50%>
</p>

The implementation is tailored for electron ptychography to enable reduction of the electron dose through adaptive scanning. It is based upon the idea that, at atomic resolution, ptychography requires an increased information redundancy through overlapping illuminating beams only at regions that contain atomic structure of the scanned specimen. Here, the scan positions are predicted sequentially during the experiment and the only information required for the prediction process is the diffraction data acquired at previous scan positions. The scan position prediction model is a mixture of deep learning models and the model training is performed with both supervised and reinforcement learning.

## Model Description

A recurrent neural network (RNN) is used for the generation of scan sequences. Its network architecture is designed to model temporal sequences with recurring input information:

<p align="center">
 <img src="./misc/RNN.png" alt="Drawing", width=40%>
</p>

At the predicted scan positions, diffraction patterns are acquired by the microscope and from these diffraction patterns a partial potential $V_t(\textbf{r})$ is reconstructed using the gradient based ptychography reconstruction algorithm [ROP](https://opg.optica.org/oe/fulltext.cfm?uri=oe-28-19-28306&id=438009).

For the processing of the partial reconstruction $V_t(\textbf{r})$ that is the basis for the compressed representation $z_t$ of the RNN, we make use of a convolutional autoencoder:

<p align="center">
 <img src="./misc/Autoencoder.png" alt="Drawing", width=40%>
</p>

For a more detailed description of the model, see the reference given below.

## Results

<p align="center">
 <img src="./misc/Results.png" alt="Drawing", width=40%>
</p>

The result of adaptive scanning on experimentally acquired MoS$_2$ data and its comparison to the result of a sparse grid scanning and the conventional grid scanning procedure is shown below. In our comparison, a ground truth reconstruction is obtained consisting of 10,000 diffraction patterns, while only $250$ diffraction patterns of this data set have been used for the adaptive scanning as well as the sparse grid scanning reconstruction. Figure a) shows the ptychographic reconstruction when using a sparse grid scanning procedure. The structure of the material is not clearly resolved and/or shows ambiguous features. Figure b) shows the reconstruction when the scan positions are predicted through adaptive scanning. Although without the same homogeneous reconstruction quality throughout the entire field of view, the structure of the MoS$_2$ material is now much better resolved and is closer to the ground truth reconstruction of the full data grid scanning procedure, shown in Figure c). The inset illustrates the illumination probe used for the reconstruction with an estimated size of 0.093nm. The pixel size is identical to the one used in the reconstruction and its magnitude is represented by the intensity, and the phase is represented by the HSV colorwheel. d) Fourier Ring Correlation of the sparse grid scan and the adaptive scan averaged over 25 data sets and where the standard deviation is illustrated by the shaded area.

The data set is found here: [dataset](https://doi.org/10.5281/zenodo.7088721).

### System Requirement

+ software
    + gcc (7.4.0)
    + g++ (7.4.0)
    + CUDA (10.0)
    + python 3.5+
    + pytorch 0.3+
    + tqdm 4.56.0
    + argparse 1.1
    + scipy 1.7.0
    + pycuda 2020.1

+ recommanded hardware
    + CPUs: Intel(R) Xeon(R) Gold 6136 @ 3.00GHz, 24 Cores; 
    + GPUs: NVIDIA V100 16GB/32GB, 5120 Cores
    + 2T hard disk space
    + 187 GB Memory

## Usage

Before using the adaptive scanning model, parameters have to be specified in the 'config.ini' file. When using ROP as reconstruction algorithm, the files 'params_part.cnf' and 'params_full.cnf' that are found in the reconstruction folder, need to be adapted to the experimental settings as well. The source code for the ROP algorithm is also provided in the reconstruction folder and for the compilation you can follow the description given in the repository [ROP-DEMO](https://gitlab.com/Schlozma/rop-demo). Once you have specified the parameters, start the training with the following command:

```
python main.py config.ini
```
The output will include either the loss or reward function at every iteration and the updated network weights at the specified interval of iterations.

## Reference

We kindly ask you to cite our publication if you use our dataset, code or models in your work.

```
@article{schloz2023deep,
  title={Deep reinforcement learning for data-driven adaptive scanning in ptychography},
  author={Schloz, Marcel and M{\"u}ller, Johannes and Pekin, Thomas C and Van den Broek, Wouter and Madsen, Jacob and Susi, Toma and Koch, Christoph T},
  journal={Scientific Reports},
  volume={13},
  number={1},
  pages={8732},
  year={2023},
  publisher={Nature Publishing Group UK London}
}

@misc{dataset,
  title={Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography},
  author={Schloz, Marcel and Müller, Johannes and Pekin, Thomas Christopher and Van den Broek, Wouter and Koch, Christoph Tobias},
  publisher={Zenodo},
  note={Zenodo (2022), \url{https://doi.org/10.5281/zenodo.7088721}} }

```

