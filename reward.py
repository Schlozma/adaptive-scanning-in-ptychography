'''
Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography
written by Marcel Schloz (schlozma@hu-berlin.de)
all rights reserved
'''

import os, struct, subprocess, glob, torch
import numpy as np
from scipy import spatial

import pycuda.driver as cuda
from pycuda.compiler import SourceModule

def create_seeds(M_d, S_x_d, S_y_d, sequences, jfa_dimension, total_seeds, Params, mod, blockDimMS, gridDimMS):
    """
    A function that calculates the closest pixels in the reconstruction grid to the predicted scan positions.

    Args
    ----
    - jfa_dimension: the dimension of the JFA frame in which the reconstruction is inserted.
    - Params: object containing information about dimension and pixel size of the reconstruction.

    Return
    ----
    - M_d: a segmentation map that allocates each pixel to a Voronoi cell.
    - S_x_d: a pixel index map that allocates each pixel to the 
      x-coordinate of the seed of each Voronoi cell.
    - S_y_d: a pixel index map that allocates each pixel to the 
      y-coordinate of the seed of each Voronoi cell.
    """
    #Create the grid of pixels
    recon_max = (jfa_dimension / 2) * Params.reconstruction_pixel_size
    recon_min = -(jfa_dimension / 2) * Params.reconstruction_pixel_size
    yrange = np.arange(recon_min, recon_max, Params.reconstruction_pixel_size)
    xrange = np.arange(recon_min, recon_max, Params.reconstruction_pixel_size)
    ppX, ppY = np.meshgrid(xrange, yrange)
    coord_grid = np.reshape(np.transpose(np.array([ppX, ppY]), (1, 2, 0)), (jfa_dimension * jfa_dimension, 2))
    #Retrieve the predicted scan positions (in [m]) from the position file
    positions = np.loadtxt(Params.recon_path + "/positions.txt")
    positions = positions[sequences]
    positions = np.delete(positions, 0, axis=2)
    positions = np.reshape(positions,(Params.batch_size * Params.sequence_length, 2))
    #Find the nearest neighbour pixels of the predicted scan positions
    NN = spatial.KDTree(coord_grid).query(positions)[1]
    NN = NN.astype(np.int32)
    NN_d = cuda.mem_alloc(NN.nbytes)
    cuda.memcpy_htod(NN_d, NN)
    #Insert the nearest neighbours in the segmentation and index maps
    Insert_Nearest_Neighbour_Kernel = mod.get_function("Insert_Nearest_Neighbour")
    Insert_Nearest_Neighbour_Kernel(NN_d, M_d, S_x_d, S_y_d, jfa_dimension, total_seeds, block=blockDimMS, grid=gridDimMS)  
    #Free the allocated memory
    NN_d.free()

    return M_d, S_x_d, S_y_d


def create_kernel_dim(dimension, total_seeds, batch_size):
    """
    A function that calculates the grid and block sizes required for the CUDA kernel functions.

    Args
    ----
    - jfa_dimension: the dimension of the JFA frame in which the reconstruction is inserted.
    - Params: object containing information about dimension and pixel size of the reconstruction.

    Return
    ----
    - V_tmp: a 3D numpy array of shape (B, H, W). The minibatch
      of reconstructions.
    """
    maxThreads = np.sqrt(1024)

    if (dimension < maxThreads):
        maxThreadsJFA = dimension
    else:
        maxThreadsJFA = np.ceil(dimension/np.ceil(dimension/maxThreads))

    bJFA = int(min(dimension,maxThreadsJFA))
    gJFA = int(np.ceil(dimension/maxThreadsJFA))

    if (total_seeds < maxThreads):
        maxThreadsMS = total_seeds
    else:
        maxThreadsMS = np.ceil(total_seeds/np.ceil(total_seeds/maxThreads))

    bMS = int(min(total_seeds,maxThreadsMS))
    gMS = int(np.ceil(total_seeds/maxThreadsMS))

    return (bJFA,bJFA,1), (gJFA,gJFA,batch_size), (bMS,1,1), (gMS,1,batch_size)


def create_potential(jfa_dimension, Params):
    """
    A function that performs a reconstruction with the gradient based ptychography
    reconstruction algorithm ROP for a batch of input data.

    Args
    ----
    - jfa_dimension: the dimension of the JFA frame in which the reconstruction is inserted.
    - Params: object containing information about dimension and pixel size of the reconstruction.

    Return
    ----
    - V_tmp: a 3D numpy array of shape (B, H, W). The minibatch
      of reconstructions.
    """
    #Execute the reconstruction algorithm
    os.chdir(Params.recon_path)
    subprocess.call([Params.recon_path + "/ROP", Params.recon_path + "/params_full_0.cnf", Params.recon_path + "/data_full_0.bin", Params.recon_path +  "/psi"])
    os.chdir(Params.process_path)
    #Delete the input data files used for the reconstruction
    for adap_file in glob.glob(Params.recon_path + "/data_full_*.bin"):
        os.remove(adap_file)
    #Open the generated reconstructions which are stored in one file
    V_file = open(Params.recon_path + "/PotentialReal5.bin", "rb")
    V_data = V_file.read()
    V_file.close()
    #Create a numpy array and reshape
    V = np.reshape(np.array(struct.unpack( Params.batch_size * Params.object_dimension * Params.object_dimension * 'f', V_data)), (Params.batch_size, Params.object_dimension, Params.object_dimension))
    #Create the frame with the dimension required for the JFA algorithm
    V_tmp = np.zeros((Params.batch_size,jfa_dimension,jfa_dimension))
    #Calculate the difference between the dimension of the JFA frame and the reconstruction
    Ddim = int( (jfa_dimension - Params.object_dimension) / 2 )
    #Insert the reconstruction in the JFA frame
    V_tmp[:,Ddim:-Ddim,Ddim:-Ddim] = V

    return V_tmp


def create_step_lengths(dimension):
    """
    A function that specifies the dimension of the JFA frame (requires to be 2^x)
    and sets the step lengths (n/2,n/4,...,1) at which pixels in the frame are 
    flooded with the seed pixel values.

    Args
    ----
    - dimension: the dimension of the reconstruction.

    Return
    ----
    - dimension: the dimension of the Voronoi frame.
    - K_Lengths: a list with the step lengths of the flooding.
    """
    step = 0
    K_Lengths = []
    while 2**step < dimension:
        K_Lengths.insert(0, 2**step)
        step += 1
    jfa_dimension = 2**(step)
    return np.int32(jfa_dimension), K_Lengths


def generate_reward(sequences, Params):
    """
    A function that generates a Voronoi diagram with the Jump Flooding Algorithm (JFA) [1]
    using the predicted scan positions as seeds. The Voronoi diagram is overlayed on the
    reconstruction and the phase is summed for each Voronoi cell resulting in a reward for 
    each predicted scan position.

    Args
    ----
    - sequences: the dimension of the JFA frame in which the reconstruction is inserted.
    - Params: object containing information about dimension and pixel size of the reconstruction.

    Return
    ----
    - V_tmp: a 3D numpy array of shape (B, H, W). The minibatch
      of reconstructions.

    References
    ----------
    - [1] Rong et. al., https://dl.acm.org/doi/abs/10.1145/1111411.1111431
    """
    #Steps necessary to get CUDA ready for submission of compute kernels
    cuda.init()
    device = cuda.Device(0)
    ctx = device.make_context()
    #Define CUDA kernel functions to perform the computationally intensive processing on the GPU
    mod = SourceModule("""

        __global__ void Insert_Nearest_Neighbour(int *NN, int *M, int *S_x, int *S_y, int dimension, int total_seeds)
        {
            int idx = threadIdx.x + blockIdx.x * blockDim.x; 
            int batchId = blockIdx.z * dimension * dimension;
            int seedBatchId = blockIdx.z * total_seeds;
            int seedIx = 0;
            int s_x, s_y;
            
            if(idx < total_seeds)
            {  
                seedIx = NN[idx + seedBatchId];
                    
                s_x = (int)(seedIx % dimension);
                s_y = (int)((seedIx - s_x) / dimension);
                        
                M[seedIx + batchId] = idx;
                S_x[seedIx + batchId] = s_x;
                S_y[seedIx + batchId] = s_y;
            }
        }


        __global__ void Multiple_Seeds(int *Seeds, float *Reward, int total_seeds)
        {
            int idx = threadIdx.x + blockIdx.x * blockDim.x; 
            int seedBatchId = blockIdx.z * total_seeds;
            float tmp_reward = Reward[idx + seedBatchId];
            int cnt = 1;
            
            if(idx < total_seeds)
            {  
                for(int ix = 0; ix < total_seeds; ix++)
                {
                    if(idx != ix && Seeds[idx + seedBatchId] == Seeds[ix + seedBatchId])
                    {
                        tmp_reward += Reward[ix + seedBatchId];
                        cnt += 1;
                    }
                }
                
                __syncthreads();
                
                Reward[idx + seedBatchId] = tmp_reward / cnt;
            }
        }


        __global__ void JFA_Step(int *M, int *S_x, int *S_y, int *K_Lengths, float *V, float *Reward, int total_seeds, int dimension, int last)
        {
             int x = threadIdx.x + blockIdx.x * blockDim.x;
             int y = threadIdx.y + blockIdx.y * blockDim.y;
             int batchId = blockIdx.z * dimension * dimension;
             int seedBatchId = blockIdx.z * total_seeds;

             //if(x < dimension && y < dimension)
             if(x > 63 && x < 193 && y > 63 && y < 193)
             {
                 int bestM = M[x + dimension * y + batchId];
                 int bestS_x = S_x[x + dimension * y + batchId];
                 int bestS_y = S_y[x + dimension * y + batchId];
                 float bestDist = sqrt( (float)((bestS_x - x) * (bestS_x - x)) + (float)((bestS_y - y) * (bestS_y - y)) );

                 if(bestM == 0)
                 {
                     bestDist = 1e12;
                 }

                 for(int ky = 0; ky < 3; ky++)
                 {
                     for(int kx = 0; kx < 3; kx++)
                     {
                         int Dy = K_Lengths[ky];
                         int Dx = K_Lengths[kx];

                         if( x+Dx >= 0 && x+Dx < dimension &&  y+Dy >= 0 && y+Dy < dimension)
                         {
                             if(S_x[(x+Dx) + dimension * (y+Dy) + batchId] == 0 && S_y[(x+Dx) + dimension * (y+Dy) + batchId] == 0)
                             {continue;}

                             float dist = sqrt( (float)((S_x[(x+Dx) + dimension * (y+Dy) + batchId] - x) * (S_x[(x+Dx) + dimension * (y+Dy) + batchId] - x)) + (float)(( S_y[(x+Dx) + dimension * (y+Dy) + batchId] - y) * ( S_y[(x+Dx) + dimension * (y+Dy) + batchId] - y)) );

                             if(bestDist > dist)
                             {
                                 bestM = M[(x+Dx) + dimension * (y+Dy) + batchId];
                                 bestS_x = S_x[(x+Dx) + dimension * (y+Dy) + batchId];
                                 bestS_y = S_y[(x+Dx) + dimension * (y+Dy) + batchId];
                                 bestDist = dist;
                             }
                         }
                     }
                 }

                 __syncthreads();

                 M[x + dimension * y + batchId] = bestM;
                 S_x[x + dimension * y + batchId] = bestS_x;
                 S_y[x + dimension * y + batchId] = bestS_y;

                 if(last == 0 )
                 {atomicAdd(&(Reward[bestM + seedBatchId]), V[x + dimension * y + batchId]);}
             }
        }
        """)

    #Specify the dimension of the JFA frame and the step lengths of the flooding
    jfa_dimension, K_Lengths = create_step_lengths(Params.object_dimension)
    #Specify the total # of seeds, i.e. the scan sequence length
    total_seeds = np.int32(Params.sequence_length)
    #Create the grid and block sizes for the CUDA kernel functions
    blockDimJFA, gridDimJFA, blockDimMS, gridDimMS = create_kernel_dim(jfa_dimension, total_seeds, Params.batch_size)
    #Allocate memory on device for the index of the [-K_Length, 0, K_Length] pixel neighbours 
    K = np.array([0,0,0]).astype(np.int32)
    K_d = cuda.mem_alloc(K.nbytes)
    #Allocate memory on device for the pixel segmentation map
    M = np.zeros((Params.batch_size, jfa_dimension * jfa_dimension))
    M = M.astype(np.int32)
    M_d = cuda.mem_alloc(M.nbytes)
    cuda.memcpy_htod(M_d, M)
    #Allocate memory on device for the pixel indexing in x-direction
    S_x = np.zeros((Params.batch_size, jfa_dimension * jfa_dimension))
    S_x = S_x.astype(np.int32)
    S_x_d = cuda.mem_alloc(S_x.nbytes)
    cuda.memcpy_htod(S_x_d, S_x)
    #Allocate memory on device for the pixel indexing in y-direction
    S_y = np.zeros((Params.batch_size, jfa_dimension * jfa_dimension))
    S_y = S_y.astype(np.int32)
    S_y_d = cuda.mem_alloc(S_y.nbytes)
    cuda.memcpy_htod(S_y_d, S_y)
    #Insert the seeds of each Voronoi cell in the segmentation and index maps
    M_d, S_x_d, S_y_d = create_seeds(M_d, S_x_d, S_y_d, sequences, jfa_dimension, total_seeds, Params, mod, blockDimMS, gridDimMS)
    #Allocate memory on device for the scan positions
    Seeds = sequences.astype(np.int32)
    Seeds_d = cuda.mem_alloc(Seeds.nbytes)
    cuda.memcpy_htod(Seeds_d, Seeds)
    #Perform reconstructions and store it on device memory
    V = create_potential(jfa_dimension, Params)
    V = V.astype(np.float32)
    V_d = cuda.mem_alloc(V.nbytes)
    cuda.memcpy_htod(V_d, V)
    #Allocate memory on device for the reward of each scan position
    Reward = np.zeros(total_seeds * Params.batch_size)
    Reward = Reward.astype(np.float32)
    Reward_d = cuda.mem_alloc(Reward.nbytes)
    cuda.memcpy_htod(Reward_d, Reward)
    #Retrieve flooding step CUDA kernel function
    JFA_Step_Kernel = mod.get_function("JFA_Step")
    #Set a flag to 1 if last step length is not yet reached
    last = np.int32(1)
    #Flood the values of the pixels in the frame at a changing step length
    for Round, K_Length in enumerate(K_Lengths):
        K = np.array([-K_Length,0,K_Length]).astype(np.int32)
        cuda.memcpy_htod(K_d, K)
	#Set a flag to 0 if last step length is reached
        if(K_Length == K_Lengths[-1]):
            last = np.int32(0)
	#Perform the flooding step
        JFA_Step_Kernel(M_d, S_x_d, S_y_d, K_d, V_d, Reward_d, total_seeds, jfa_dimension, last, block=blockDimJFA, grid=gridDimJFA)
    #Retrieve multiple seed CUDA kernel function
    Multiple_Seeds_Kernel = mod.get_function("Multiple_Seeds")
    #Check for multiple seeds at the same position and normalize reward for these seeds accordingly 
    Multiple_Seeds_Kernel(Seeds_d, Reward_d, total_seeds, block=blockDimMS, grid=gridDimMS)
    #Transfer the calculated reward from device to host
    cuda.memcpy_dtoh(Reward, Reward_d)
    Reward = np.reshape(Reward, (Params.batch_size, Params.sequence_length))
    Reward = np.reshape( Reward[:, int(Params.sequence_length / Params.subsequences):] , (Params.batch_size, int(Params.subsequences - 1), int(Params.sequence_length / Params.subsequences)))
    #Free all allocated memory on the device
    M_d.free()
    S_x_d.free()
    S_y_d.free()
    Seeds_d.free()
    K_d.free()
    V_d.free()
    Reward_d.free()
    #Remove the created context
    ctx.pop()

    return torch.from_numpy(np.array(Reward)).float()

