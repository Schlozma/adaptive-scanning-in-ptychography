'''
Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography
written by Marcel Schloz (schlozma@hu-berlin.de)
all rights reserved
'''

import os, subprocess, struct, scipy.ndimage, torch
import numpy as np

from auxiliary import denormalize_sequence

def rewriteParams(sequence, batch, Params):
    """
    A function that generates the parameter file for the reconstruction algorithm
    by writing down the beam positions according to the predicted scan positions.

    Args
    ----
    - sequence: a 3D numpy array of shape (B, S, 2). The scan positions given as numbering.
    - data: a 3D numpy array of shape (B, S, 2). The scan positions (normalized [x,y]-coordinates).
    - Params: object containing information about dimension and pixel size of the reconstruction.
    - seq_part: bool. 
    """
    #Read in the text file with all existing beam positions (in [m]) in the experiment
    positions = np.loadtxt(Params.recon_path +"/positions.txt")
    positions = positions.T
    #Read in the parameter file for the reconstruction and create a temporary file
    txt_file_in = open(Params.recon_path + "/params_part.cnf", "r")
    txt_file_out = open(Params.recon_path +"/Temp" + str(batch) + ".cnf", "a")
    txt_file_adap_out = open(Params.recon_path +"/params_full_" + str(batch) + ".cnf", "a")
    #Copy the parameters from the parameter file to the temporary file
    for line in txt_file_in:
        if(line[:4]!='beam'):
            txt_file_out.write(line)
            #Add the beam positions according to the predicted scan positions to the temporary file
            if(line[:10]=='#x- and y-'):
                for pos in range(int(Params.subsequence_length)):
                    x_n = "{:.4e}".format( positions[1][sequence[pos]] )
                    y_n = "{:.4e}".format( positions[2][sequence[pos]] )
                    txt_file_out.write('beam_position:   ' + str(x_n) + '\t' + str(y_n) + '\n')
                    txt_file_adap_out.write('beam_position:   ' + str(x_n) + '\t' + str(y_n) + '\n')
    #Copy the parameters from the temporary file to the parameter file
    txt_file_out = open(Params.recon_path +"/Temp" + str(batch) + ".cnf", "r")
    txt_file_in = open(Params.recon_path +"/params_part_" + str(batch) + ".cnf", "w")
    text=txt_file_out.read()
    txt_file_in.write(text)
    os.remove(Params.recon_path +"/Temp" + str(batch) + ".cnf")


def generateReconFile(sequence, dps, batch, Params):
    """
    A function that generates the input data file for the reconstruction algorithm
    by selecting the diffraction patterns according to the predicted scan positions.

    Args
    ----
    - sequence: a 3D numpy array of shape (B, S, 2). The scan positions given as numbering.
    - data: a 3D numpy array of shape (B, S, 2). The scan positions (normalized [x,y]-coordinates).
    - Params: object containing information about dimension and pixel size of the reconstruction.
    - seq_part: bool. 
    """
    #Specify path for the data file of the intermediate reconstruction
    recon_file_path = Params.recon_path +"/data_part_" + str(batch) + ".bin"
    #Specify path for the data file of the complete reconstruction
    adap_file_path = Params.recon_path + "/data_full_" + str(batch) + ".bin"
    if os.path.exists(recon_file_path):
        os.remove(recon_file_path)
    recon_file = open(recon_file_path, "ab")
    adap_file = open(adap_file_path, "ab")
    #Select the diffraction patterns based on the predicted scan positions
    dp_sequence = []
    for pos in range(int(Params.subsequence_length)):
        dp_sequence.append(dps[batch,sequence[pos],:,:])
    dp_sequence = np.ravel(np.array(dp_sequence))
    dp_sequence = struct.pack(Params.subsequence_length * Params.dp_dimension * Params.dp_dimension * 'f', *dp_sequence)
    #Add the diffraction patterns to the data files
    recon_file.write(dp_sequence)
    recon_file.close()
    adap_file.write(dp_sequence)
    adap_file.close()


def generateProbeFile(psi_re, psi_im, Params):
    probe_re_path = Params.recon_path +"/psi_re.bin"
    probe_im_path = Params.recon_path + "/psi_im.bin"
    if(os.path.isfile(probe_re_path) == True):
        os.remove(probe_re_path)
    if(os.path.isfile(probe_im_path) == True):
        os.remove(probe_im_path)

    for batch in range(Params.batch_size):
        probe_re_file = open(probe_re_path, "ab")
        probe_im_file = open(probe_im_path, "ab")
        probe_re_input = np.ravel(psi_re[batch])
        probe_re_input = struct.pack(Params.probe_dimension*Params.probe_dimension*'f', *probe_re_input)
        probe_im_input = np.ravel(psi_im[batch])
        probe_im_input = struct.pack(Params.probe_dimension*Params.probe_dimension*'f', *probe_im_input)
        probe_re_file.write(probe_re_input)
        probe_re_file.close()
        probe_im_file.write(probe_im_input)
        probe_im_file.close()


def partial_reconstruction(locs, scan_sequences, dps, psis_re, psis_im, Params, seq_part):
    """
    A function that performs a reconstruction with the gradient based ptychography
    reconstruction algorithm ROP for a batch of input data.

    Args
    ----
    - locs: a 3D numpy array of shape (B, S, 2). The scan positions (normalized [x,y]-coordinates).
    - scan_sequences: a 3D numpy array of shape (B, S, 2). The scan positions given as numbering.
    - dps: a 3D numpy array of shape (B, S, 2). The scan positions (normalized [x,y]-coordinates).
    - Params: object containing information about dimension and pixel size of the reconstruction.
    - seq_part: bool. 

    Return
    ----
    - X_pred: a 4D Tensor of shape (B, H, W, C). The minibatch
      of predicted images.
    """
    sequences_batch = []

    for batch in range(Params.batch_size):
	#Convert scan positions from coordinates to numbering
        seqs = denormalize_sequence(locs, batch, Params)
	#Generate the parameter files used for the reconstruction with ROP
        rewriteParams(seqs, batch, Params)
	#Generate the input probe file used for the reconstruction with ROP
        if seq_part == 0:
            generateProbeFile(psis_re.data.numpy(), psis_im.data.numpy(), Params)
	#Generate the input data file used for the reconstruction with ROP
        generateReconFile(seqs, dps.data.numpy(), batch, Params)
        sequences_batch.append(seqs)

    if(seq_part == 0):
        scan_sequences = sequences_batch
    else:
        scan_sequences = np.hstack( (scan_sequences, np.array(sequences_batch)))

    recon_batch = []

    if(seq_part < (Params.subsequences - 1) ):
	    #Execute the reconstruction algorithm
            os.chdir(Params.recon_path)
            subprocess.call([Params.recon_path + "/ROP", Params.recon_path + "/params_part_0.cnf", Params.recon_path + "/data_part_0.bin", Params.recon_path +  "/psi"])
            os.chdir(Params.process_path)
	    #Retrieve the real part of the generated reconstruction
            recon_real = open(Params.recon_path + "/PotentialReal5.bin", "rb").read()
            recon_real = np.reshape(np.array(struct.unpack(Params.batch_size * Params.object_dimension * Params.object_dimension * 'f', recon_real)),(Params.batch_size, Params.object_dimension, Params.object_dimension))
	    #Retrieve the imaginary part of the generated reconstruction
            recon_imag = open(Params.recon_path + "/PotentialImag5.bin", "rb").read()
            recon_imag = np.reshape(np.array(struct.unpack(Params.batch_size * Params.object_dimension * Params.object_dimension * 'f', recon_imag)), (Params.batch_size, Params.object_dimension, Params.object_dimension))
	    #Normalize the reconstruction
            recon_mean_re = np.mean(np.mean(recon_real, axis=1), axis=1)
            recon_std_re = np.std(np.std(recon_real, axis=1), axis=1)
            recon_mean_im = np.mean(np.mean(recon_imag,axis=1), axis=1)
            recon_std_im = np.std(np.std(recon_imag,axis=1), axis=1)
	    #Convert the reconstruction to the input size of the autoencoder
            factor = Params.reconstruction_pixel_size / Params.frame_pixel_size
            for batch in range(Params.batch_size):
                recon_real[batch,:,:] = (recon_real[batch,:,:] - recon_mean_re[batch] ) / recon_std_re[batch]
                recon_imag[batch,:,:] = (recon_imag[batch,:,:] - recon_mean_im[batch] ) / recon_std_im[batch]
                recon_real_zoomed = scipy.ndimage.zoom(recon_real[batch], factor, order=3)
                recon_imag_zoomed = scipy.ndimage.zoom(recon_imag[batch], factor, order=3)
                aux = 0
                if(recon_real_zoomed.shape[0] % 2 != 0):
                    aux = 1
                space = int( (Params.input_dimension - np.floor(recon_real_zoomed.shape[0]) ) / 2 )
                zeros_re = np.zeros((Params.input_dimension, Params.input_dimension), dtype=float)
                zeros_re[space:-(space+aux), space:-(space+aux)] += recon_real_zoomed
                zeros_im = np.zeros((Params.input_dimension, Params.input_dimension), dtype=float)
                zeros_im[space:-(space+aux), space:-(space+aux)] += recon_imag_zoomed
                recon_batch.append([zeros_re, zeros_im])

    return torch.from_numpy(np.array(recon_batch)).float(), scan_sequences


