'''
Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography
written by Marcel Schloz (schlozma@hu-berlin.de)
all rights reserved
'''

import numpy as np
import os, sys, pickle, torch
from tqdm import tqdm
from torch.utils.data import Dataset

from auxiliary import resample, normalize_sequence

import configparser
import argparse


class autoencoder_model_params:
    def __init__(self, config):
        self.input_dimension = int(config['autoencoder_model_params']['input_dimension'])
        self.frame_pixel_size = float(config['autoencoder_model_params']['frame_pixel_size'])
        self.kernel_sizes = eval(config['autoencoder_model_params']['kernel_sizes'])
        self.channels = eval(config['autoencoder_model_params']['channels'])
        super(autoencoder_model_params,self).__init__(config)
   
    
class rnn_model_params:
    def __init__(self, config):
        self.sequence_length = int(config['rnn_model_params']['sequence_length'])
        self.subsequences = int(config['rnn_model_params']['subsequences'])
        self.subsequence_length = int(self.sequence_length / self.subsequences)
        self.hidden_size = int(config['rnn_model_params']['hidden_size'])
        self.stacked_layers = int(config['rnn_model_params']['stacked_layers'])
        self.policy_sigma = float(config['rnn_model_params']['policy_sigma'])
        super(rnn_model_params,self).__init__(config)


class optimization_params:
    def __init__(self, config):
        self.learning_rate = float(config['optimization_params']['learning_rate'])
        self.iterations = int(config['optimization_params']['iterations'])
        self.save_interval = int(config['optimization_params']['save_interval'])
        self.batch_size = int(config['optimization_params']['batch_size'])
        super(optimization_params,self).__init__(config)


class reconstruction_params:
    def __init__(self, config):
        self.object_dimension = int(config['reconstruction_params']['object_dimension'])
        self.probe_dimension = int(config['reconstruction_params']['probe_dimension'])
        self.dp_dimension = int(config['reconstruction_params']['dp_dimension'])
        self.reconstruction_pixel_size = float(config['reconstruction_params']['reconstruction_pixel_size'])
        self.grid_scan_positions = int(config['reconstruction_params']['grid_scan_positions'])
        self.grid_scan_step = float(config['reconstruction_params']['grid_scan_step'])
        super(reconstruction_params,self).__init__(config)


class execution_params:
    def __init__(self, config):
        self.execution_model = config['execution_params']['execution_model']
        self.execution_task = config['execution_params']['execution_task']
        self.data_path = config['execution_params']['data_path']
        self.recon_path = config['execution_params']['recon_path']
        self.save_path = config['execution_params']['save_path']
        self.process_path = os.getcwd()


class Parameters(autoencoder_model_params, rnn_model_params, optimization_params, reconstruction_params, execution_params):
    def __init__(self):

        parser = argparse.ArgumentParser()
        parser.add_argument("config_file", help="insert the config.ini file")
        args = parser.parse_args()
        config = configparser.ConfigParser()
        config.read(args.config_file)

        super(Parameters, self).__init__(config)

        self.checkParams()

    def checkParams(self):
        assert (self.input_dimension * self.frame_pixel_size > self.object_dimension * self.reconstruction_pixel_size), "Size[m] of autoencoder input should be larger than size[m] of reconstruction!"
        assert (len(self.kernel_sizes) == len(self.channels)), "Number of kernel sizes must be equal to number of output channels!"
        for k_size in self.kernel_sizes:
            assert(k_size % 2 == 1), "All kernel sizes must be of odd dimension!"
        if not os.path.exists(self.data_path):
            raise FileNotFoundError("Data path not found!")
        if not os.path.exists(self.save_path):
            raise FileNotFoundError("Save path not found!")
        if not os.path.exists(self.save_path + "/network_weights"):
            os.makedirs(self.save_path + "/network_weights")
        if not os.path.exists(self.recon_path):
            raise FileNotFoundError("Reconstruction path not found!")
        if not os.path.exists(self.data_path):
            raise FileNotFoundError("Data path for adapty not found!")


class autoencoder_data_loading(Dataset):

    def __init__(self, Params):

        self.groundtruths = []
        self.tests = []

        for folder in tqdm(os.listdir(Params.data_path + "/training_data"), file=sys.stdout):
            folder_path = os.path.join(Params.data_path + "/training_data", folder)
            #Read and store reconstructions into memory
            with open(folder_path + "/DataFile.pkl", 'rb') as df:
                tst_file, gt_file = resample( pickle.load( df ), Params )
                self.tests.append( torch.from_numpy( tst_file ).float() )
                self.groundtruths.append( torch.from_numpy( gt_file ).float() )

        print("Data fully loaded! Starting algorithm ...", flush = True)

    def __len__(self):
        return len(self.groundtruths)

    def __getitem__(self, index):
        groundtruth = self.groundtruths[index]
        test = self.tests[index]

        return (groundtruth, test)


class rnn_data_loading(Dataset):
    "Read in the dataset of reconstructions and store in memory"

    def __init__(self, Params):
        self.locs = []
        self.dps = []
        self.psis_re = []
        self.psis_im = []

        for folder in tqdm(os.listdir(Params.data_path + "/training_data"), file=sys.stdout):
            folder_path = os.path.join(Params.data_path + "/training_data", folder)
            with open(folder_path + "/DataFile.pkl", 'rb') as df:
                df = pickle.load(df)
                self.locs.append(normalize_sequence(np.load(Params.data_path + "/sequence_grid.npy"), Params))
                self.dps.append(df['dps'])
                self.psis_re.append(df['psi_real'])
                self.psis_im.append(df['psi_imag'])

        print("Data fully loaded! Starting algorithm ...", flush = True)

    def __len__(self):
        return len(self.locs)

    def __getitem__(self, index):
        locs = self.locs[index]
        dps = self.dps[index]
        psis_re = self.psis_re[index]
        psis_im = self.psis_im[index]

        return (locs, dps, psis_re, psis_im)

