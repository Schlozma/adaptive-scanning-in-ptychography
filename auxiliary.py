'''
Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography
written by Marcel Schloz (schlozma@hu-berlin.de)
all rights reserved
'''

import torch
import shutil
import numpy as np
import scipy.ndimage

def toint(f):
    trunc = int(f)
    diff = f - trunc

    # trunc is one too low
    if abs(f - trunc - 1) < 0.00001:
        return trunc + 1
    # trunc is one too high
    if abs(f - trunc + 1) < 0.00001:
        return trunc - 1
    # trunc is the right value
    return trunc


def denormalize_sequence(locations, batch, Params):

    locations_num = np.zeros_like(locations.detach().data.numpy())

    for pos in range(int(Params.subsequence_length)):

        if (locations[batch, pos, 0].data.numpy() < -1):
            locations_num[batch,pos,0] = -1
        elif (locations[batch, pos,0].data.numpy() > 0.96):
            locations_num[batch, pos, 0] = 0.96
        else:
            locations_num[batch, pos, 0] = locations[batch, pos,0].data.numpy()

        locations_num[batch,pos,0] = toint(0.5 * (locations_num[batch,pos,0] + 1) * Params.grid_scan_positions )

        if (locations[batch, pos, 1].data.numpy() < -1):
            locations_num[batch, pos, 1] = -1
        elif (locations[batch, pos, 1].data.numpy() > 0.96):
            locations_num[batch, pos, 1] = 0.96
        else:
            locations_num[batch, pos, 1] = locations[batch, pos, 1].data.numpy()

        locations_num[batch,pos,1] = toint( 0.5 * (locations_num[batch,pos,1] + 1)  * Params.grid_scan_positions )

    # create x- and y- coordinates
    x = locations_num[batch][:,0]
    y = locations_num[batch][:,1]
    sequence = np.array( y * Params.grid_scan_positions + x, dtype="int")

    return sequence


def center(pos, Params):
    x = pos % Params.grid_scan_positions
    y = (pos - x) / Params.grid_scan_positions

    return int(x), int(y)


def normalize_sequence(sequence, Params):

    normalized_sequence = []

    for pos in range(int(Params.sequence_length)):

        x, y = center(sequence[pos], Params)
        x /= Params.grid_scan_positions
        y /= Params.grid_scan_positions
        x = x/0.5 - 1
        y = y/0.5 - 1
        normalized_sequence.append([x, y])

    normalized_sequence = np.reshape( np.array( normalized_sequence ), (Params.sequence_length, 2) )

    return normalized_sequence


def initialize(locations_batch, Params):

    init_state = torch.zeros(Params.stacked_layers, Params.batch_size, Params.hidden_size)
    init_locations = locations_batch[:,:int(Params.subsequence_length),:].float()
    duplicate_param_files(Params)

    return init_locations, init_state


def duplicate_param_files(Params):

    for batch in range(Params.batch_size):
        shutil.copy2(Params.recon_path + "/params_full.cnf", Params.recon_path + "/params_full_" +str(batch) + ".cnf")


def resample(data_dict, Params):
    """
    Sample the reconstruction according to the training data used for the autoencoder and back.

    Args
    ----
    - V: a 3D numpy array of shape (2, W, H). The input reconstruction (flag == 0) or the resampled predicted reconstruction (flag == 1).
    - flag: bool. 
    - Params: object containing information about dimension and pixel size of the reconstruction.

    Return
    ----
    - V: a 3D numpy array of shape (2, W, H). The resampled reconstruction (flag == 0) or the predicted reconstruction (flag == 1).
    """

    factor = Params.reconstruction_pixel_size / Params.frame_pixel_size
    #Normalize input
    data_mean_re = np.mean(data_dict['tst_real'])
    data_std_re = np.std(data_dict['tst_real'])
    data_mean_im = np.mean(data_dict['tst_imag'])
    data_std_im = np.std(data_dict['tst_imag'])

    data_dict['tst_real'] = (data_dict['tst_real'] - data_mean_re)/data_std_re
    data_dict['tst_imag'] = (data_dict['tst_imag'] - data_mean_im)/data_std_im
    data_dict['gt_real'] = (data_dict['gt_real'] - data_mean_re)/data_std_re
    data_dict['gt_imag'] = (data_dict['gt_imag'] - data_mean_im)/data_std_im

    tst_re = scipy.ndimage.zoom(data_dict['tst_real'], factor, order=3)
    tst_im = scipy.ndimage.zoom(data_dict['tst_imag'], factor, order=3)

    gt_re = scipy.ndimage.zoom(data_dict['gt_real'], factor, order=3)
    gt_im = scipy.ndimage.zoom(data_dict['gt_imag'], factor, order=3)

    aux = 0
    if(tst_re.shape[0] % 2 != 0):
        aux = 1

    space = int( (Params.input_dimension - np.floor(tst_re.shape[0]) ) / 2 )

    tst_out_re = np.zeros((Params.input_dimension, Params.input_dimension))
    tst_out_re[space:-(space+aux), space:-(space+aux)] += tst_re
    tst_out_im = np.zeros((Params.input_dimension, Params.input_dimension))
    tst_out_im[space:-(space+aux), space:-(space+aux)] += tst_im

    gt_out_re = np.zeros((Params.input_dimension, Params.input_dimension))
    gt_out_re[space:-(space+aux), space:-(space+aux)] += gt_re
    gt_out_im = np.zeros((Params.input_dimension, Params.input_dimension))
    gt_out_im[space:-(space+aux), space:-(space+aux)] += gt_im

    return np.array([tst_out_re, tst_out_im]), np.array([gt_out_re, gt_out_im])


def save_results(metric, model, Params, it, sequence = None):
    
    #Save model weights
    model.save(it)
    
    if Params.execution_model == 'autoencoder':
        #Save the loss
        file = open(Params.save_path + '/loss.txt','a')
        line = str(it) + '\t' + str(metric)  + '\n'
        file.write(line)
        file.close()

    if Params.execution_model == 'imitator':
        #Save the loss
        file = open(Params.save_path + '/loss.txt','a')
        line = str(it) + '\t' + str(metric)  + '\n'
        file.write(line)
        file.close()
        #Save predicted scan position sequence
        np.save(Params.save_path + "/sequence.npy", sequence)

    if Params.execution_model == 'tuner':
        #Save the reward
        metric = np.mean(np.mean( np.sum( metric.detach().data.numpy(), axis=2), axis=1))
        file = open(Params.save_path + '/reward.txt','a')
        line = str(it) + '\t' + str(metric) + '\n'
        file.write(line)
        file.close()
        #Save predicted scan position sequence
        np.save(Params.save_path + "/sequence.npy", sequence)
