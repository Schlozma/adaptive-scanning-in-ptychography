'''
Deep Reinforcement Learning for Data-Driven Adaptive Scanning in Ptychography
written by Marcel Schloz (schlozma@hu-berlin.de)
all rights reserved
'''

import torch
from initialization import Parameters
from training import run_training, run_testing

if __name__ == "__main__":

    torch.cuda.current_device()
    Params = Parameters()
    if Params.execution_task == 'training':
        run_training(Params)
    if Params.execution_task == 'testing':
        run_testing(Params)
